#!/usr/bin/python3
import os
import sys
import nmap
import subprocess
import time
import shlex
import nmap
import callbacks
import global_variables
import curses
import sqlite3
import db
import socket
import threading


def execute_scan(cmd):
    return

def execute_nmap(cmd):
    n = nmap.PortScannerAsync()
    n.scan(arguments = cmd[4:], callback = callbacks.nmap_callback)
    # need to add a reference here because, if not, the object gets deleted
    # after we return
    global_variables.active_scans.append(n)
    return

def execute_nmap_old(cmd):
    command = shlex.split(cmd)
    out = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    (stdout, stderr) = out.communicate()
    while True:
        if (out.poll() is None):
            time.sleep(1)
        else:
            print(stdout.decode('utf-8'))
            break
    return


def execute_listen(cmd):
    c = cmd.split()
    if (len(c) < 2):
        print("Provide port number")
        return
    try:
        port = int(c[1])
    except:
        print("Invalid port number")
        return

    s = socket.socket()
    s.setblocking(False)
    try:
        s.bind(("", port))
        s.listen(1)
        global_variables.listeners.append(s)
    except OSError as er:
        print(er)
        return

    return

def execute_netcat(cmd):
    command = shlex.split(cmd)
    out = subprocess.Popen(command, stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    return

def execute_exit(cmd):
    print("Exiting...")
    for s in global_variables.listeners:
        s.shutdown(socket.SHUT_RDWR)
        s.close()
    for s in global_variables.sessions:
        s.shutdown(socket.SHUT_RDWR)
        s.close()
    sys.exit()
    return


def execute_show(cmd):
    c = cmd.split()
    if (len(c) > 1):
        execute_option(cmd)
        return

    print("Select option:")
    print(" [l] listeners")
    print(" [s] sessions")
    print(" [q] return")
    print("")
    option = input("option > ")
    cmd = cmd + " " + option
    execute_option(cmd)
    return

def execute_option(cmd):
    c = cmd.split()
    if (c[1] == 'q'):
        return
    if (c[1] == 'l' or c[1] == "listeners" or c[1] == "listening"):
        execute_show_listeners()
        return
    if (c[1] == "s" or c[1] == "sessions" or c[1] == "session"):
        execute_show_sessions()

    return

def execute_show_sessions():
    print("Current sessions:")
    for session in global_variables.sessions:
        rhost, rport = session.getsockname()
        print(rhost + ":" + str(rport))

    return

def execute_show_listeners():
    print("Current listeners:")
    for listener in global_variables.listeners:
        lhost, lport = listener.getsockname()
        print(lhost + ":" + str(lport))

    return

def execute(cmd):
    c = cmd.split()
    if (len(c) == 0):
        return
  
    if (c[0] == "scan"):
        execute_scan(cmd)
        return

    if (c[0] == "nmap"):
        execute_nmap(cmd)
        return


    if (c[0] == "nmap-old"):
        execute_nmap_old(cmd)
        return

    if (c[0] == "listen" or c[0] == "l"):
        execute_listen(cmd)
        return

    if (c[0] == "nc" or c[0] == "netcat"):
        execute_netcat(cmd)
        return

    if (c[0] == "options" or c[0] == "o"):
        execute_options(cmd)
        return
   


    '''
    Single element input
    '''
    if (c[0] == "show" or c[0] == 's'):
        execute_show(cmd)
        return
    
    if (c[0] == "sl"):
        execute_show_listeners()
        return

    if (c[0] == "ss"):
        execute_show_sessions()

    if (c[0] == "q" or c[0] == "quit" or c[0] == "exit"):
        execute_exit(cmd)
        return


def update_internal_state():
    accepted_connections = []
    for s in global_variables.listeners:
        try:
            '''
            fd, addr = s.accept()
            raddr = addr[0]
            rport = addr[1]
            laddr, lport = s.getsockname()
            global_variables.sessions.append([s, fd, raddr, rport, laddr, lport])
            '''
            sess = s.accept()
            sess[0].setblocking(False)
            global_variables.sessions.append(sess[0])
            accepted_connections.append(s)
        except:
            pass

    for s in accepted_connections:
        if (s in global_variables.listeners):
            s.close()
            global_variables.listeners.remove(s)
    
    # checking timeout
    timedout_sessions = []
    for sock in global_variables.sessions:
        try:
            active = sock.recv(1024, socket.MSG_PEEK)
            if not active:
                timedout_sessions.append(sock)
        except BlockingIOError:
            pass
    
    for t in timedout_sessions:
        if t in global_variables.sessions:
            global_variables.sessions.remove(t)

    return

def main():
    db.db_init()
    #curses.wrapper(draw_menu)
    while True:
        cmd = input("liege > ")
        update_internal_state()
        execute(cmd)
    return


if __name__ == "__main__":
    banner = open("banner.txt","r")
    for line in banner:
        print(line[:-1])
    banner.close()
    main()
