import global_variables
import sqlite3

def db_init():
    con = sqlite3.connect('liege.db')
    cur = con.cursor()
    try:
        result = cur.execute('SELECT * FROM liege')
    except sqlite3.Error as er:
        print("Creating liege database")
        cur.execute("CREATE TABLE liege (instance INTEGER)")
        cur.execute("INSERT INTO liege VALUES (0)")
        cur.execute("CREATE TABLE nmap (host text, scan_data text)")
        cur.execute("CREATE TABLE hosts (ip text, hostname text)")
        cur.execute("CREATE TABLE scans (ip text, port text, protocol text, state text)")
        con.commit()

    global_variables.db_con = con
    global_variables.db_cur = cur

    return

